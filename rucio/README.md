# Instructions for USTC Users Needing CMS Data

- USTC users who need CMS data must create a text file containing sample names. An example of such a file is located in `txtfiles/forLou.txt`. You can use the [Data Aggregation System (DAS)](https://cmsweb.cern.ch/das/request?view=list&limit=50&instance=prod%2Fglobal&input=%2F*%2FRun3Winter23*%2F*RAW) or the `dasgoclient -query` command to find the necessary sample names.
- Ensure you have "write" and "execute" permissions for `/ustcfs3/cms/store` to be able to download to the `store`. If you do not have the necessary permissions, please do not hesitate to open an issue [here](https://gitlab.cern.ch/ustc-cms/ustc-support/-/issues) or send your list to @nlu. 
- `/ustcfs3/cms/store` is not a CMS Tier site, so it's important to keep all CMS data in one place to avoid duplicates in users' home directories. This helps save storage space and makes `/ustcfs3/cms/store` more efficient.

## Setup environment:
```bash
cd /ustcfs3/cms/ustc-support/rucio
source setup.sh
```
## Run locally ( only for small datasize ):
```bash
python3 download_samples.py --das txtfiles/forLou.txt --requestsmp 
```
### Detailed usage and help:
```bash
usage: download_samples.py [-h] [-o OUTDIR] --das DAS [--stoarge STOARGE] [--lazy] [--condor] [--overwrite] [--requestsmp] [--resubmit]

Download samples from DAS and manage them with various options.
optional arguments:
  -o OUTDIR, --outdir OUTDIR
                        jobs file to be stored in
  -t TIMEOUT, --timeout TIMEOUT
                        Sets the send/recieve timeout for the gfal command
  --das DAS              a Text file of datasets path
  --stoarge STOARGE      datasets to be stored in
  --lazy                Multiple replicas exist for single dataset, if this flagged true, will try only one replica
  --condor              Submit to HTCondor: 1 job per dataset
  --overwrite           If  the root file exist will pass
  --requestsmp          Will copy DAS to the /stoarge, otherwise will only prepare the repliacs requests in /ustcfs3/cms/request
  --resubmit            Try to copy once more, advised to set overwrite=True, and keep lazy=False
```
### Write bamboo analysis configuration:
- You can also write the `.yml` plotit configuration needed for [bamboo](https://bamboo-hep.readthedocs.io/en/latest/) using the same `.txt` file and by running:
```bash
python3 WriteYaml.py --das txtfiles/forLou.txt
```
### Check files status and resubmit truncated ones:
The script `download_samples.py` generates a list of replicas for each specified dataset, saving them in the `request/.json` directory. If a site is inaccessible, the script might attempt to fetch all replicas without success. Consequently, the root files for the given dataset might be incomplete.
You can identify these zombie files by running the script below and giving them another chance to be copied using the `--resubmit` option.
```bash
python3 findZombies.py
python3 download_samples.py --das txtfiles/Zombie_files_2.txt --resubmit
```
## Run on HTCondor:
```bash
# do the necessary changes to submit_to_condor.sh  by adding your .txt file , keep requestsmp=true
source setup.sh
./submit_to_condor.sh
```
## Dealing with failed jobs:
You can use `finalize=true` in the `submit_to_condor.sh` script to check for any failed jobs. If there are any failures, the script will provide you with a suggested command for resubmitting those jobs to HTCondor.

## Available samples in the store:
- Please check `request/` directory or find the availbale datasets on ustcwebpage: [https://ustccms.docs.cern.ch](https://ustccms.docs.cern.ch).
