#/NMSSM_XtoYHto4B_MX-1400_MY-400_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
| SCOPE:NAME                                                                                                                                                                                                | GUID   | ADLER32     | FILESIZE   | EVENTS   |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------|
| cms:/store/mc/Run3Summer22EENanoAODv12/NMSSM_XtoYHto4B_MX-1400_MY-400_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/60000/665e37fb-53a8-4366-ac42-868a54eb9c48.root | (None) | ad:d28c41b0 | 4.820 MB   |          |
| cms:/store/mc/Run3Summer22EENanoAODv12/NMSSM_XtoYHto4B_MX-1400_MY-400_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/60000/4a81cd14-76ce-4eca-b959-082c79c75b9f.root | (None) | ad:66f14da5 | 226.009 MB |          |
| cms:/store/mc/Run3Summer22EENanoAODv12/NMSSM_XtoYHto4B_MX-1400_MY-400_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/60000/5e043177-17d6-4f75-b44b-66c5bb912c33.root | (None) | ad:bf568c06 | 4.843 MB   |          |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
Total files : 3
Total size : 235.673 MB
