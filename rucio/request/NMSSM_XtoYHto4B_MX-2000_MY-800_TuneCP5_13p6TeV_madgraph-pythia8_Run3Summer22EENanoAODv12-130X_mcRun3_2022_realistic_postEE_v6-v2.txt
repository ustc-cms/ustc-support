#/NMSSM_XtoYHto4B_MX-2000_MY-800_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
| SCOPE:NAME                                                                                                                                                                                                | GUID   | ADLER32     | FILESIZE   | EVENTS   |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------|
| cms:/store/mc/Run3Summer22EENanoAODv12/NMSSM_XtoYHto4B_MX-2000_MY-800_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/60000/6f2ce7e5-f1d9-4f8a-a4d0-9ed863b8f3db.root | (None) | ad:00b1580c | 243.165 MB |          |
| cms:/store/mc/Run3Summer22EENanoAODv12/NMSSM_XtoYHto4B_MX-2000_MY-800_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/60000/96b65cdc-6269-4698-8cd7-2fd03e5c12b6.root | (None) | ad:3edf6c2b | 4.965 MB   |          |
| cms:/store/mc/Run3Summer22EENanoAODv12/NMSSM_XtoYHto4B_MX-2000_MY-800_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/60000/8e208b71-6c32-4e25-8dd7-d7f5da9d160d.root | (None) | ad:317989dd | 4.974 MB   |          |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
Total files : 3
Total size : 253.104 MB
