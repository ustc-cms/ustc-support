#/NMSSM_XtoYHto4B_MX-1200_MY-400_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer23BPixNanoAODv12-130X_mcRun3_2023_realistic_postBPix_v6-v2/NANOAODSIM
+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
| SCOPE:NAME                                                                                                                                                                                                     | GUID   | ADLER32     | FILESIZE   | EVENTS   |
|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------|
| cms:/store/mc/Run3Summer23BPixNanoAODv12/NMSSM_XtoYHto4B_MX-1200_MY-400_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_postBPix_v6-v2/140000/a1e89c85-6b18-4d75-83ec-fe6082e147c4.root | (None) | ad:a52d5031 | 12.073 MB  |          |
| cms:/store/mc/Run3Summer23BPixNanoAODv12/NMSSM_XtoYHto4B_MX-1200_MY-400_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_postBPix_v6-v2/140000/08b7915a-b9b1-4bd5-8310-ef9c63807bbf.root | (None) | ad:5f5c1e9e | 29.395 MB  |          |
| cms:/store/mc/Run3Summer23BPixNanoAODv12/NMSSM_XtoYHto4B_MX-1200_MY-400_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_postBPix_v6-v2/140000/b7519dd1-47f7-487c-9a1b-7673d978682a.root | (None) | ad:fcf579ef | 64.099 MB  |          |
+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
Total files : 3
Total size : 105.568 MB
