import glob 
import os
import sys

htmlscript_1 = """<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

#myInput {
  background-image: url('https://cdn-icons-png.flaticon.com/512/622/622669.png'); /* Search icon */
  background-position: 10px 12px;
  background-repeat: no-repeat;
  background-size: 20px;
  width: 100%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

#myUL {
  list-style-type: none;
  padding: 0;
  margin: 0;
}

#myUL li a {
  border: 1px solid #ddd;
  margin-top: -1px;
  background-color: #f6f6f6;
  padding: 12px;
  text-decoration: none;
  font-size: 16px;
  color: black;
  display: block;
  word-wrap: break-word;
  white-space: normal;
  overflow-wrap: break-word;
}

#myUL li a:hover:not(.header) {
  background-color: #eee;
}

mark {
  background-color: yellow;
  color: black;
  font-weight: bold;
}
</style>
</head>
<body>

<h2>Available Datasets on USTC storage: <code>/ustcfs3/cms/store/</code></h2>

<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for dataset.." title="Type in a dataset name">

<ul id="myUL">
"""


htmlscript_2 = """
</ul>
<footer style="margin-top: 20px; text-align: center; font-size: 14px; color: #555;">
    <p>© 2025 University of Science and Technology of China. All rights reserved.</p>
    <p>Last updated: <span id="last-updated"></span></p>
    <p>Source code: <a href="https://gitlab.cern.ch/ustc-cms/ustc-support" target="_blank">GitLab Repository</a></p>
</footer>

<script>
function myFunction() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

// Set the last updated timestamp
document.getElementById("last-updated").textContent = new Date().toLocaleString();
</script>

</body>
</html>
"""



# Output HTML file
fout = f"/ustcfs3/cms/ustc-support/public/index.html"
with open(fout, "w") as f:
    sys.stdout = f  # Redirect output to file
    print(htmlscript_1)

    for i, req in sorted(enumerate(glob.glob(os.path.join("/ustcfs3/cms/", 'request', '*.json')))):
    
        dataset = '/'+req.split('/')[-1].replace('.json', '').replace('_Run', '/Run')
        txtfile = req.split('/')[-1].replace('.json', '.txt')
        if any(dataset.startswith(f'/{x}') for x in ['Muon', 'Parking', 'JetHT', 'MuonEG', 'DoubleEG', 'EGamma', 'DoubleMuon', 'SingleMuon', 'SingleElectron']):
            dataset +='/NANOAOD'
        else:
            dataset +='/NANOAODSIM'
        dataset_url = f"https://gitlab.cern.ch/ustc-cms/ustc-support/-/blob/master/rucio/request/{txtfile}"        
        print(f"""<li><a href="{dataset_url}" target="_blank" rel="noopener noreferrer"><code>{i+1}.{dataset}</code></a></li>""")
    
    print( htmlscript_2)


sys.stdout = sys.__stdout__  # Restore stdout before printing message
print(f'Website successfully updated, check: {fout} before commit...')
