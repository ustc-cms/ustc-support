#!/bin/bash

# Initialize variables for optional arguments
DAS_FILE=""
REQUESTSMP=""
RESUBMIT=""
OVERWRITE=""
OUTDIR=""
CONDOR=""
FINALIZE=""
TIMEOUT="1800"
# Parse additional arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        --das)
            DAS_FILE=$2
            shift 2
            ;;
        --requestsmp)
            REQUESTSMP="--requestsmp"
            shift
            ;;
        --finalize)
            FINALIZE="--finalize"
            shift
            ;;
        --resubmit)
            RESUBMIT="--resubmit"
            shift
            ;;
        --overwrite)
            OVERWRITE="--overwrite"
            shift
            ;;
        --outdir)
            OUTDIR=$2
            shift 2
            ;;
        --condor)
            CONDOR="--condor"
            shift
            ;;
        --timeout)
            TIMEOUT=$2
            shift 2
            ;;
        -h|--help)
            echo "Usage: $0 --das DAS_FILE [--requestsmp] [--resubmit] [--overwrite] [--outdir OUTDIR] [--condor] [--timeout TIMEOUT] [--finalize FINALIZE]"
            exit 0
            ;;
        *)
            echo "Unknown option: $1"
            shift
            ;;
    esac
done
# Check if DAS_FILE is set
if [ -z "$DAS_FILE" ]; then
    echo "Error: --das is a required argument."
    exit 1
fi

# Source the CMS environment setup script
_BASE=/cvmfs/grid.cern.ch/centos7-umd4-ui-4.0.3-1_191004
source /cvmfs/cms.cern.ch/cmsset_default.sh
source $_BASE/etc/profile.d/setup-c7-ui-example.sh
source /cvmfs/cms.cern.ch/rucio/setup-py3.sh
# Source the necessary environment
source $HOME/ustcfs3/cms/py3venv/bin/activate

export RUCIO_ACCOUNT=`whoami`

# Add the directory containing voms-proxy-init to the PATH
#export PATH=$_BASE/usr/bin:$PATH

# Ensure the voms-proxy-init command is available
VOMS_PROXY_INIT_CMD=$(which voms-proxy-init)
if [ -x "$VOMS_PROXY_INIT_CMD" ]; then
    # Use the existing proxy certificate
    #export X509_USER_PROXY=$(pwd)/x509up_u$(id -u) does not work on htcondor
    export X509_USER_PROXY=$HOME/x509up_u$RUCIO_ACCOUNT
else
   echo "Error: voms-proxy-init command not found."
    exit 1
fi

# Verify that X509_CERT_DIR is set correctly
echo "X509_CERT_DIR is set to: $X509_CERT_DIR"
echo "X509_USER_PROXY is set to: $X509_USER_PROXY"

# Prepare the command
pushd /ustcfs3/cms/ustc-support/rucio/
echo $pwd
CMD="python3 download_samples.py --das $DAS_FILE --timeout $TIMEOUT"

if [ -n "$REQUESTSMP" ]; then
    CMD="$CMD $REQUESTSMP"
fi

if [ -n "$FINALIZE" ]; then
    CMD="$CMD $FINALIZE"
fi

if [ -n "$RESUBMIT" ]; then
    CMD="$CMD $RESUBMIT"
fi

if [ -n "$OVERWRITE" ]; then
    CMD="$CMD $OVERWRITE"
fi

if [ -n "$OUTDIR" ]; then
    CMD="$CMD --outdir $OUTDIR"
fi

if [ -n "$CONDOR" ]; then
    CMD="$CMD $CONDOR"
fi

# Print the command for debugging purposes
echo "Running command: $CMD"
# Execute the command
$CMD
