+-------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
| SCOPE:NAME                                                                                                        | GUID   | ADLER32     | FILESIZE   | EVENTS   |
|-------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------|
| cms:/store/data/Run2022C/DoubleMuon/NANOAOD/ReRecoNanoAODv11-v1/2540000/29f4073a-59c9-4f4e-825e-28b8ddaf765c.root | (None) | ad:967e46c5 | 1.236 GB   |          |
| cms:/store/data/Run2022C/DoubleMuon/NANOAOD/ReRecoNanoAODv11-v1/2540000/d278c568-362e-409c-a7dd-713c1a3c20eb.root | (None) | ad:2056070e | 509.692 MB |          |
| cms:/store/data/Run2022C/DoubleMuon/NANOAOD/ReRecoNanoAODv11-v1/50000/8a349b75-d8fa-4cba-af55-46f15e77092f.root   | (None) | ad:d0089c2f | 229.608 MB |          |
| cms:/store/data/Run2022C/DoubleMuon/NANOAOD/ReRecoNanoAODv11-v1/2540000/20d7bf11-4b01-4fbd-9001-3d779c98d71e.root | (None) | ad:31356f16 | 14.696 MB  |          |
| cms:/store/data/Run2022C/DoubleMuon/NANOAOD/ReRecoNanoAODv11-v1/2540000/b95ae0b1-745f-42cc-848a-6d0858b3ffa8.root | (None) | ad:51393e38 | 171.189 MB |          |
| cms:/store/data/Run2022C/DoubleMuon/NANOAOD/ReRecoNanoAODv11-v1/2540000/8d5a45aa-2441-4271-aa27-969ec0c9cab6.root | (None) | ad:ebdfc27b | 129.467 MB |          |
| cms:/store/data/Run2022C/DoubleMuon/NANOAOD/ReRecoNanoAODv11-v1/2540000/a7084fc5-d33e-4ac1-bb90-5ee95b85a353.root | (None) | ad:a476e9f4 | 1.110 GB   |          |
| cms:/store/data/Run2022C/DoubleMuon/NANOAOD/ReRecoNanoAODv11-v1/2540000/4219c6c3-c18e-428d-ba7a-9a38dfb8960a.root | (None) | ad:f01ea6a5 | 321.948 MB |          |
+-------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
Total files : 8
Total size : 3.723 GB
