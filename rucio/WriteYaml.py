#!/usr/bin/env python3

import sys, os, os.path
import yaml
import argparse
import logging
import coloredlogs

from concurrent.futures import ThreadPoolExecutor, as_completed
from collections import defaultdict, OrderedDict
from faker import Factory
fake = Factory.create()

logger = logging.getLogger('DasLoad')
coloredlogs.install(level='DEBUG', logger=logger, fmt='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


LumiCertifications={ "2022":
                {"Total":"https://cms-service-dqmdc.web.cern.ch/CAF/certification/Collisions22/Cert_Collisions2022_355100_362760_Golden.json",
                 "B":"https://cms-service-dqmdc.web.cern.ch/CAF/certification/Collisions22/Cert_Collisions2022_eraB_355100_355769_Golden.json",
                 "C":"https://cms-service-dqmdc.web.cern.ch/CAF/certification/Collisions22/Cert_Collisions2022_eraC_355862_357482_Golden.json",
                 "D":"https://cms-service-dqmdc.web.cern.ch/CAF/certification/Collisions22/Cert_Collisions2022_eraD_357538_357900_Golden.json",
                 "E":"https://cms-service-dqmdc.web.cern.ch/CAF/certification/Collisions22/Cert_Collisions2022_eraE_359022_360331_Golden.json",
                 "F":"https://cms-service-dqmdc.web.cern.ch/CAF/certification/Collisions22/Cert_Collisions2022_eraF_360390_362167_Golden.json",
                 "G":"https://cms-service-dqmdc.web.cern.ch/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json",
                 },
               }
# https://twiki.cern.ch/twiki/bin/viewauth/CMS/PdmVRun3Analysis#DATA_AN3
Run_ranges = {'2022':{  'A':[352416, 354787],
                        'B':[355100, 355769],
                        'C':[355862, 357482],
                        'D':[357538, 357900],
                        'E':[359022, 360331],
                        'F':[360390, 362167],
                        'G':[362433, 362760],
                      }   
                    }

def get_systematics():
    return ['jer', 'jes']

# Define the configuration data as a nested dictionary
config_data ={
    'tree': 'Events',
    'eras': {
        # '2018UL': {
        #     'luminosity': 59830.0,
        #     'luminosity-error': 0.015
        # },
        # '2017UL': {
        #     'luminosity': 41480.0,
        #     'luminosity-error': 0.015
        # },
        # '2016ULpostVFP': {
        #     'luminosity': 16800.0,
        #     'luminosity-error': 0.015
        # },
        # '2016ULpreVFP': {
        #     'luminosity': 19500.0,
        #     'luminosity-error': 0.015
        # },
        '2022': {
            'luminosity': 7980.4,
            'luminosity-error': 0.015
        },
        '2022EE': {
            'luminosity': 26671.7,
            'luminosity-error': 0.015
        }
    },
    'dbcache': 'dascache',
    'plotIt': {
        'configuration': {
            'width': 800,
            'height': 800,
            'luminosity-label': '%1$.2f fb^{-1} (13 TeV)',
            'experiment': 'CMS',
            'extra-label': 'run3 --Work in progress',
            'show-overflow': True,
            'blinded-range-fill-style': 4050,
            'blinded-range-fill-color': '#FDFBFB',
            'y-axis-format': '%1% / %2$.2f'
        },
        'legend': {
            'position': [0.45, 0.5, 0.95, 0.89],
            'columns': 3
        },
        'groups': {
            'data': {
                'legend': 'data'
            },
            'signal': {
                'legend': 'Signal'
            },
        },
        'plotdefaults': {
            'y-axis': 'Events',
            'log-y': 'both',
            'y-axis-show-zero': True,
            'save-extensions': ['pdf', 'png'],
            'show-ratio': True,
            'ratio-y-axis-range': [0.6, 1.4],
            'sort-by-yields': False
        },
        'systematics': get_systematics()
    }
}

# https://cms-analysis.docs.cern.ch/guidelines/plotting/colors/
groups_and_colorpalettes = {
    ## for hh -> 4b 
    (0, 'QCD', '#e76300'): ['QCD-4Jets'],
    (1, 'tt', '#964a8b'): ['TT', 'TTto2L2Nu', 'TTto4Q', 'TTtoLNu2Q'],
    (2, 'VV', '#92dadd'): ['WW', 'ZZ', 'WZ'],
    (3, 'VVV', '#9c9ca1'): ['WZZ', 'WWZ', 'WWW', 'ZZZ'],
    (4, 'Vh', '#bd1f01'): ['ZH', 'WplusH', 'WminusH'],
    (5, 'ggH', '#3f90da'): ['GluGluHto2B'],
    (6, 'tth', '#832db6'): ['TTHto2B'],
    (7, 'VBF', '#a96b59'): ['VBFHto2B'],
    (8, 'W+jets', '#ffa90e'): ['Wto2Q-3Jets'],
    ## for h -> 2mu 
    (0, 'DY+jets', '#5F9EA0'): ['DYto2L-2Jets'],
}

def add_groups(found):
    for  k, v in groups_and_colorpalettes.items():
        order, group_name, color = k 
        if not group_name in found:
            continue
        config_data['plotIt']['groups'].update({ group_name: {
                 'fill-color': f'{color}',
                 'legend': group_name,
                 'order': order,
                 'line-width': 1,
                 'line-color': 1,
                 'line-style': 1
                 }})

def get_group(g):
    for k, v in groups_and_colorpalettes.items():
        if g in v:
            return k
    raise KeyError(f"Group '{g}' not found in groups_and_colorpalettes")


def get_crosssections(smp): # in pb 
    ustc_analysis_samples_xsc = {
            ## h --> 2mu 
            "DYto2L-2Jets_MLL-50_0J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8": 5378,
            "DYto2L-2Jets_MLL-50_1J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8": 1017,
            "DYto2L-2Jets_MLL-50_2J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8": 385.5,
            "ZZZ_TuneCP5_13p6TeV_amcatnlo-pythia8": 0.01591,
            "WZZ_TuneCP5_13p6TeV_amcatnlo-pythia8": 0.06206,
            "WWZ_4F_TuneCP5_13p6TeV_amcatnlo-pythia8": 0.1851,
            "WWW_4F_TuneCP5_13p6TeV_amcatnlo-madspin-pythia8": 0.2328,
            ## hh --> 4b 
            "GluGluHto2B_PT-200_M-125_TuneCP5_13p6TeV_powheg-minlo-pythia8": 0.5246,
            "GluGlutoHHto4B_kl-0p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8": 0.06648,
            "GluGlutoHHto4B_kl-0p00_kt-1p00_c2-1p00_TuneCP5_13p6TeV_powheg-pythia8": 0.1492,
            "GluGlutoHHto4B_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8": 0.02964,
            "GluGlutoHHto4B_kl-1p00_kt-1p00_c2-0p10_TuneCP5_13p6TeV_powheg-pythia8": 0.01493,
            "GluGlutoHHto4B_kl-1p00_kt-1p00_c2-0p35_TuneCP5_13p6TeV_powheg-pythia8": 0.01052,
            "GluGlutoHHto4B_kl-1p00_kt-1p00_c2-3p00_TuneCP5_13p6TeV_powheg-pythia8": 2.802,
            "GluGlutoHHto4B_kl-1p00_kt-1p00_c2-m2p00_TuneCP5_13p6TeV_powheg-pythia8": 1.875,
            "GluGlutoHHto4B_kl-2p45_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8": 0.01252,
            "GluGlutoHHto4B_kl-5p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8": 0.08664,
            "QCD-4Jets_HT-40to70_TuneCP5_13p6TeV_madgraphMLM-pythia8": 311400000,
            "QCD-4Jets_HT-70to100_TuneCP5_13p6TeV_madgraphMLM-pythia8": 58500000,
            "QCD-4Jets_HT-100to200_TuneCP5_13p6TeV_madgraphMLM-pythia8": 27990000,
            "QCD-4Jets_HT-200to400_TuneCP5_13p6TeV_madgraphMLM-pythia8": 1961000,
            "QCD-4Jets_HT-400to600_TuneCP5_13p6TeV_madgraphMLM-pythia8": 95620,
            "QCD-4Jets_HT-600to800_TuneCP5_13p6TeV_madgraphMLM-pythia8": 13540,
            "QCD-4Jets_HT-800to1000_TuneCP5_13p6TeV_madgraphMLM-pythia8": 3033,
            "QCD-4Jets_HT-1000to1200_TuneCP5_13p6TeV_madgraphMLM-pythia8": 883.7,
            "QCD-4Jets_HT-1200to1500_TuneCP5_13p6TeV_madgraphMLM-pythia8": 383.5,
            "QCD-4Jets_HT-1500to2000_TuneCP5_13p6TeV_madgraphMLM-pythia8": 125.2,
            "QCD-4Jets_HT-2000_TuneCP5_13p6TeV_madgraphMLM-pythia8": 26.49,
            "TTto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8": 762.1,
            "TTto4Q_TuneCP5_13p6TeV_powheg-pythia8": 762.1,
            "TTtoLNu2Q_TuneCP5_13p6TeV_powheg-pythia8": 762.1,
            "TT_TuneCP5_13p6TeV_powheg-pythia8": 762.3,
            "TTHto2B_M-125_TuneCP5_13p6TeV_powheg-pythia8": 0.5742,
            "ttHto2B_M-125_TuneCP5_13p6TeV_powheg-pythia8": 0.5742,
            "WW_TuneCP5_13p6TeV_pythia8": 80.23,
            "WZ_TuneCP5_13p6TeV_pythia8": 29.1,
            "WminusH_Hto2B_Wto2Q_M-125_TuneCP5_13p6TeV_powheg-pythia8": 0.3916,
            "WplusH_Hto2B_Wto2Q_M-125_TuneCP5_13p6TeV_powheg-pythia8": 0.623,
            "ZH_Hto2B_Zto2Q_M-125_TuneCP5_13p6TeV_powheg-pythia8": 0.5958,
            "ZZ_TuneCP5_13p6TeV_pythia8": 12.75,
            "ggZH_Hto2B_Zto2Q_M-125_TuneCP5_13p6TeV_powheg-pythia8": 0.04776,
            "VBFHto2B_M-125_TuneCP5_13p6TeV_powheg-pythia8": 4.18,
            "Wto2Q-3Jets_HT-200to400_TuneCP5_13p6TeV_madgraphMLM-pythia8": 2723.0,
            "Wto2Q-3Jets_HT-400to600_TuneCP5_13p6TeV_madgraphMLM-pythia8": 299.8,
            "Wto2Q-3Jets_HT-600to800_TuneCP5_13p6TeV_madgraphMLM-pythia8": 63.9, 
            "Wto2Q-3Jets_HT-800_TuneCP5_13p6TeV_madgraphMLM-pythia8":31.9,
            }
    if smp in ustc_analysis_samples_xsc.keys():
        return ustc_analysis_samples_xsc[smp]
    else:
        return 1.

def get_era_sampleName(daspath):
    smp = daspath
    if isinstance(daspath, list): smp = daspath[0]
    if "Run3Summer22EENanoAODv12" in smp or "postEE" in daspath[0]: era = "2022EE"
    elif "Run3Summer22NanoAODv12" in smp : era = "2022"
    elif any( x in smp for x  in ['Run2022B', 'Run2022C', 'Run2022D']): era = "2022"
    elif any( x in smp for x  in ['Run2022E', 'Run2022F', 'Run2022G' ]): era = "2022EE" 
    else:
        raise ValueError(f"No matching era found for daspath: {daspath}")
    return era +'UL'

def add_comment(d, key, comment):
    if isinstance(d, dict) and key in d:
        d[key] = (d[key], comment)
    return d
                    
def get_signal_parameters(key):
    p = []
    for i in range(1,4):
        p +=[float(key.split('_')[i].split('-')[-1].replace('p','.'))]
    return p

def process_paths(file_path, version):
    all_groups = []
    paths_dict = defaultdict(dict)
    with open(file_path, 'r') as file:
        for daspath in file:
            daspath = daspath.strip()
            if daspath:  # Check if the daspath is not empty
                # Split the path to get the key
                parts = daspath.split('/')
                era = get_era_sampleName(daspath)
                _type = 'mc' if 'NANOAODSIM' in daspath else 'data'
                
                key = f'{parts[1]}'
                if _type == 'data':
                    key = '{}'.format('_'.join(daspath.split('/')[1:3])).split('-')[0]
                if 'GluGlutoHHto4B_kl-' in daspath:
                    _type = 'signal'
                    kappa_lambda, kappa_t, c2 = get_signal_parameters(key)
                    legend ='"(k_{#lambda},k_{t},c_{2})=(%s,%s,%s)"'%(kappa_lambda, kappa_t, c2)
                     
                if version == 'compact':
                    # Add the path to the dictionary with additional details
                    if key not in paths_dict:
                        paths_dict[key]['dbs'] = {}
                        paths_dict[key]['type'] = _type
                        if _type in ['mc', 'signal']:
                            paths_dict[key]['generated-events'] = "genEventSumw"
                            paths_dict[key]['cross-section'] = get_crosssections(key) # Adjust this as necessary"
                            if _type == 'signal':
                                paths_dict[key]['legend'] = f'{legend}'
                                paths_dict[key]['line-color'] = fake.hex_color()
                                paths_dict[key]['line-type'] = 8 # dashed
                                paths_dict[key]['line-width'] = 3
                            else: 
                                gr =get_group(f"{key.split('_')[0]}")[1]  # Adjust this as necessary"
                                if not gr in all_groups:
                                    all_groups.append(gr)
                                paths_dict[key]['group'] = gr 
                        else:
                            run = daspath.split('/')[2].split('-')[0][-1]
                            run_range = Run_ranges[era[:4]][run]
                            paths_dict[key]['group'] = 'data'  
                            paths_dict[key]["run_range"] = run_range
                            paths_dict[key]["certified_lumi_file"] = LumiCertifications[era[:4]]['Total']
                        paths_dict[key]['split'] = 2  
                    if not era in paths_dict[key]['dbs'].keys():
                         paths_dict[key]['dbs'][era] =[] 
                    # Add the specific dataset path to the 'dbs' entry
                    print( era, daspath )
                    paths_dict[key]['dbs'][era] += [f'das:{daspath}']

                elif version == 'default':
                    # Add the path to the dictionary with additional details
                    key = f'{key}_{era}'
                    if key not in paths_dict:
                        paths_dict[key]['db'] = []
                        paths_dict[key]['type'] = _type
                        if _type in ['mc', 'signal']:
                            paths_dict[key]['generated-events'] = "genEventSumw"
                            paths_dict[key]['cross-section'] = get_crosssections(key.replace(f'_{era}','')) # Adjust this as necessary"
                            if _type == 'signal':
                                paths_dict[key]['legend'] = legend
                                paths_dict[key]['line-color'] = fake.hex_color()
                                paths_dict[key]['line-type'] = 8 # dashed
                                paths_dict[key]['line-width'] = 3
                            else: 
                                gr =get_group(f"{key.split('_')[0]}")[1]  # Adjust this as necessary"
                                if not gr in all_groups:
                                    all_groups.append(gr)
                                paths_dict[key]['group'] = gr 
                        else:
                            run = daspath.split('/')[2].split('-')[0][-1]
                            run_range = Run_ranges[era[:4]][run]
                            paths_dict[key]['group'] = 'data'  
                            paths_dict[key]["run_range"] = run_range
                            paths_dict[key]["certified_lumi_file"] = LumiCertifications[era[:4]]['Total']
                        paths_dict[key]['split'] = 40
                        paths_dict[key]['era'] = f"{era}" 
                    # Add the specific dataset path to the 'dbs' entry
                    paths_dict[key]['db'] += [f'das:{daspath}']

    # Post-process to ensure single-entry lists are stored as strings
    if version == 'compact':
        for key in paths_dict:
            for era in paths_dict[key]['dbs']:
                if len(paths_dict[key]['dbs'][era]) == 1:
                    paths_dict[key]['dbs'][era] = paths_dict[key]['dbs'][era][0]
    elif version == 'default':
        for key in paths_dict:
            if len(paths_dict[key]['db']) == 1:
                paths_dict[key]['db']= paths_dict[key]['db'][0]
    return paths_dict, all_groups

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--das', required=True, action='store', help=' a Text files of dataset path')
    parser.add_argument('--version', required=False, action='store', default='default', choices=['default', 'compact'], 
            help='default does a duplicate of the dataset for each era,\n'
                 'while compact keep multiple keys of eras for a single dataset,\n'
                 'the latest requires a customization when running bamboo ')
    parser.add_argument('-o', '--output', required=False, action='store', help='Name of the output .yml file')

    options = parser.parse_args()

    outYaml = options.output
    if not options.output: 
        outYaml = os.path.join(options.das.replace('.txt', f'_{options.version}.yml'))

    # Process the input file
    paths_dict, found_groups = process_paths(options.das, options.version)
    
    with open(outYaml, 'w+') as yaml_file:
        yaml.dump(dict(paths_dict), yaml_file, sort_keys=False, default_flow_style=False)
    
    add_groups(found_groups)
    anaCfg = os.path.join(os.path.dirname(options.das), 'analysis.yml')
    with open(anaCfg, 'w+') as yaml_file:
        yaml.dump(config_data, yaml_file, sort_keys=False, default_flow_style=False)
    
    print(f'Data has been successfully written to {outYaml}')
    print(f'Analysis configuration has been successfully written to {anaCfg}')
