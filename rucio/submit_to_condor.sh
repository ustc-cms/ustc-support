#!/bin/bash


#========= User inputs ================
#======================================
# Define your input file
DAS_FILE='txtfiles/nmssm_x_ytoh.txt'
outdir="cmstransfer_20250217_094816" # default: "cmstransfer_$(date +%Y%m%d_%H%M%S)" 
                                     # If you plan to resubmit and finalize, you should use the same dir
timeout='24000'  # global timeout for the execution of the command. Command is interrupted if time expires before it finishes. 
overwrite=true   # useful to turn this to true if the root files are corrupted
lazy=false       # will try only 1 replica

# only 1 can be true at a time
requestsmp=false       # request full datasets
finalize=true          # check if all requested root files are transfered and not zombies
resubmit=false         # finalize will create a txt file inside outdir  with corrupted path to root files that can be used in $DAS_FILE
                       # for resubmission, command will be given in the end of the script

#=====================================
#======================================


mkdir -p "$outdir"
echo "HTCondor outputs will be saved in $outdir"

# Ensure only one of the flags is true
true_count=0

if $resubmit; then
    true_count=$((true_count + 1))
fi
if $finalize; then
    true_count=$((true_count + 1))
fi
if $requestsmp; then
    true_count=$((true_count + 1))
fi
if [ $true_count -gt 1 ]; then
    echo "Error: Only one of resubmit, finalize, or requestsmp can be true at a time."
    exit 1
fi

args=''
if $resubmit; then
    args+='--resubmit --condor'
fi
if $finalize; then
    args+='--finalize'
fi
if $requestsmp; then
    args+='--requestsmp --condor'
fi
if $overwrite; then 
    args+=' --overwrite'
fi
if $lazy; then 
    args+=' --lazy'
fi

## Ensure the voms-proxy-init command is available
VOMS_PROXY_INIT_CMD=$(which voms-proxy-init)
if [ -x "$VOMS_PROXY_INIT_CMD" ]; then
    # Use the existing proxy certificate
    #export RUCIO_ACCOUNT=`whoami`
    #export X509_USER_PROXY=$(pwd)/x509up_u$(id -u) does not work on htcondor
    #export X509_USER_PROXY=$HOME/x509up_u$RUCIO_ACCOUNT
    cp $(voms-proxy-info -p) ~/.x509_proxy
    export X509_USER_PROXY=$(realpath ~/.x509_proxy)
else
   echo "Error: voms-proxy-init command not found."
   source setup.sh 
fi

# Verify that X509_CERT_DIR is set correctly
echo "X509_CERT_DIR is set to: $X509_CERT_DIR"
echo "X509_USER_PROXY is set to: $X509_USER_PROXY"

pushd /ustcfs3/cms/ustc-support/rucio/

CMD="python3 download_samples.py --das $DAS_FILE --outdir $outdir --timeout $timeout $args"
# Print the command for debugging purposes
echo "Running command: $CMD"
# Execute the command and check if it succeeded
eval $CMD

if [ $? -eq 0 ]; then
    echo "Command executed successfully."
else
    echo "Command failed."
    exit 1
fi



# Check the current user
CURRENT_USER=$USER


# Set ACLs based on the user
if [ "$CURRENT_USER" == "nlu" ]; then
    echo "User is 'nlu'. Setting ACLs for 'khawla'..."
    # Set default ACLs for directories
    setfacl -R -m u:khawla:rwx /ustcfs3/cms/store/ &>err
    setfacl -R -m u:khawla:rwx /ustcfs3/cms/request/ &>err
    setfacl -R -d -m u:khawla:rwx /ustcfs3/cms/store/ &>err
    setfacl -R -d -m u:khawla:rwx /ustcfs3/cms/request/ &>err
elif [ "$CURRENT_USER" == "khawla" ]; then
    echo "User is 'khawla'. Setting ACLs for 'nlu'..."
    # Set default ACLs for directories
    setfacl -R -m u:nlu:rwx /ustcfs3/cms/store/ &>err
    setfacl -R -m u:nlu:rwx /ustcfs3/cms/request/ &>err
    setfacl -R -d -m u:nlu:rwx /ustcfs3/cms/store/ &>err
    setfacl -R -d -m u:nlu:rwx /ustcfs3/cms/request/ &>err
else
    echo "User '$CURRENT_USER' is not authorized to set ACLs."
    exit 1
fi

echo "ACLs set successfully."

