+----------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
| SCOPE:NAME                                                                                               | GUID   | ADLER32     | FILESIZE   | EVENTS   |
|----------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------|
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/033a4d07-3b5b-4d12-9374-5dc4ab55cc7b.root | (None) | ad:28a3487e | 1.478 GB   |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/2c5c983e-d4be-4b22-a592-15f9e7523d40.root | (None) | ad:c39acc2f | 1.567 GB   |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/3e952046-b6d8-44bf-8488-2e3c9243bf57.root | (None) | ad:39421122 | 1.441 GB   |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/40c0c8b1-4e1e-4755-8beb-36389f0a2926.root | (None) | ad:49d6b8a8 | 913.920 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/4251c73a-1d94-46c0-bd48-c4b9be62b8f5.root | (None) | ad:f722b7d8 | 828.420 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/43dd9afb-e21b-4f96-9eac-c31b66158825.root | (None) | ad:c5b2a1e8 | 435.177 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/4972082d-0e1b-48d5-a7b3-23158c0d725b.root | (None) | ad:d2b468f0 | 931.154 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/8072afbc-17eb-4f97-820b-8f84d9d1ce37.root | (None) | ad:48e4d40a | 1.075 GB   |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/bbd1509c-cf89-435d-b15c-c7f8122c4e2c.root | (None) | ad:0404aa03 | 1.265 GB   |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/d3039f31-0802-4005-8534-ed8ac6bc1e3c.root | (None) | ad:8780d89c | 1.617 GB   |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/e17bf5ec-24ea-4958-bbe4-fc73aeb2cfb1.root | (None) | ad:66a14058 | 582.740 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/ef780c64-622a-480b-8b43-56fecb7a3ef7.root | (None) | ad:9b708da6 | 207.611 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/a0d013c1-6f4d-4cb1-aede-f417f1411738.root | (None) | ad:ef09cd73 | 412.952 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/fb3a056f-74ee-4974-9e56-987f7cedcc8b.root | (None) | ad:cf5da5d6 | 853.023 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/4f2e4be1-cceb-4971-8551-670cdbdc1e1c.root | (None) | ad:a5ae33ee | 870.614 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/c3d2297d-ec80-4f11-b7c8-ac94923f5546.root | (None) | ad:6effcf8a | 373.094 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/f9e7781e-dea4-4173-bcca-5e5da0b6b506.root | (None) | ad:b56c0225 | 977.032 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/fdcf69fe-c3a5-4ba5-a29d-282fe974fdd5.root | (None) | ad:2c26ec76 | 906.322 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/075cd9a7-d98b-4b6a-bc4c-0233c62c9306.root | (None) | ad:8b3658df | 990.740 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/22fedd6d-140c-4280-b2ab-566a09040707.root | (None) | ad:0039829c | 390.831 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/6354c002-1a28-4181-871c-847173164368.root | (None) | ad:e9f42081 | 856.828 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/07e697c6-dccf-48ec-8ce5-9f1d8dd13978.root | (None) | ad:2484b025 | 974.506 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/46a44324-77c4-4f32-ac9c-ca78fdb4db74.root | (None) | ad:d36a1eee | 884.988 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/30000/6e093769-d874-47ab-88ba-8c02fc56bfb1.root | (None) | ad:580e992d | 440.565 MB |          |
| cms:/store/data/Run2023C/EGamma1/NANOAOD/22Sep2023_v3-v1/50000/d38af452-e4ed-4a5e-92e5-750a3a246b6b.root | (None) | ad:171b049a | 183.426 MB |          |
+----------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
Total files : 25
Total size : 21.458 GB
#EGamma1/Run2023C-22Sep2023_v3-v1/NANOAOD
