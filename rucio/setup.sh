#!/bin/sh

source /cvmfs/cms.cern.ch/cmsset_default.sh
source  /cvmfs/grid.cern.ch/alma9-ui-current/etc/profile.d/setup-alma9-test.sh
source /cvmfs/sft.cern.ch/lcg/views/LCG_105/x86_64-el9-gcc11-opt/setup.sh

voms-proxy-init -voms cms -rfc -valid 192:00
cp $(voms-proxy-info -p) ~/.x509_proxy
export X509_USER_PROXY=$(realpath ~/.x509_proxy)

venvdir=/ustcfs3/cms/ustc-support/rucio/ruciovenv
if [ ! -d "$venvdir" ]; then
    python3 -m venv $venvdir
fi
source $venvdir/bin/activate
source /cvmfs/cms.cern.ch/rucio/setup-py3.sh

