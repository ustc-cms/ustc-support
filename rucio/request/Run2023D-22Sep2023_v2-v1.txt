+------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
| SCOPE:NAME                                                                                                 | GUID   | ADLER32     | FILESIZE   | EVENTS   |
|------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------|
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/1684516d-b6b4-4f33-84ba-fb13e0211a29.root   | (None) | ad:6c77d1da | 565.384 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/5af9f1f4-6f7b-4dfc-8781-bff849c99d04.root   | (None) | ad:551d8655 | 873.776 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/96458783-a420-422d-8b6e-834b67c629aa.root   | (None) | ad:d2956674 | 1.530 GB   |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/aca39377-2d51-4c78-b2f7-99232051a7af.root   | (None) | ad:58a32f09 | 885.510 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/c68da566-ab3f-4a5d-9d4c-689b2d54a8f6.root   | (None) | ad:8536ada1 | 1.106 GB   |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/30000/e874c0a1-70fe-4b1d-81f1-4ef811cd96b3.root   | (None) | ad:646b9c34 | 302.754 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/30000/73ae2d0d-bd05-4b93-a91e-7a6ae5b7e139.root   | (None) | ad:37c80b90 | 110.900 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/3341ac43-a06e-4282-902b-e89cdfeef76a.root   | (None) | ad:86353813 | 939.256 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/7686eaae-1af8-4706-8007-987e8198a6e0.root   | (None) | ad:bffe229a | 872.754 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/a6ca8c09-101f-461e-b3be-9804c382985d.root   | (None) | ad:8c52c082 | 998.834 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/cc2982be-50c9-4737-b066-7280bcea955e.root   | (None) | ad:daa530d5 | 158.978 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/c84d8242-0e1a-4a8c-b246-994ca568d352.root   | (None) | ad:93b25e55 | 1.197 GB   |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/fcb2fd31-f992-4ac0-bd27-779d4908a18e.root   | (None) | ad:abba7dbd | 670.550 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/2540000/ebc44ee4-57ca-42e6-98ce-b1603fc9f052.root | (None) | ad:01dae47f | 246.948 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/bdd5b82b-b96d-4063-9887-771a79b66b67.root   | (None) | ad:244b75d7 | 657.794 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/30000/acb177b5-f302-4cdd-bf40-51eed9d1689f.root   | (None) | ad:289e497f | 152.156 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/30000/7382b195-f1bc-4781-9d63-9c3516315b56.root   | (None) | ad:a5319d9b | 111.403 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/14766b7a-c0f0-41b6-9144-210f68bf1adc.root   | (None) | ad:73472825 | 1.350 GB   |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/2ee40359-cc40-4b7b-a17c-d1a492f6e988.root   | (None) | ad:645f40f8 | 1.032 GB   |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/3cf39a56-bf3f-4716-abd1-41372212e634.root   | (None) | ad:cbe35d1d | 1.164 GB   |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/3d839127-3914-47e6-aeb2-896b24f86612.root   | (None) | ad:a7060df8 | 57.298 MB  |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/54e8040b-6c67-4773-a9bf-fbf4ac82fa26.root   | (None) | ad:7f2114ef | 856.440 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/60e40c04-9e27-4ddf-8a7d-c3a261804540.root   | (None) | ad:92acaf90 | 1.441 GB   |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/6ab2c1d9-5721-4472-841a-dda62aa5fc47.root   | (None) | ad:3a3a67c6 | 918.306 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/a7b5e923-1adf-40c5-9a63-d7ef4f914c33.root   | (None) | ad:a5a7e52a | 1.660 GB   |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/bcd2695a-c707-4e34-95f7-2fd1507f0a70.root   | (None) | ad:fa341f50 | 893.253 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/537594d6-01d8-4385-bd61-6b3b44f5a421.root   | (None) | ad:2615840d | 498.800 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/30000/4e5c46ca-fd67-46bc-8bf2-24c59375c083.root   | (None) | ad:83b5eb69 | 587.928 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/11577200-78a3-4824-8a6d-f4a4c6356a75.root   | (None) | ad:e318a472 | 844.963 MB |          |
| cms:/store/data/Run2023D/EGamma1/NANOAOD/22Sep2023_v2-v1/40000/d59a650a-ae92-42f4-b5a4-812e929af16e.root   | (None) | ad:555d41b6 | 130.733 MB |          |
+------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
Total files : 30
Total size : 22.814 GB
#EGamma1/Run2023D-22Sep2023_v2-v1/NANOAOD
