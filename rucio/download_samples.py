#!/usr/bin/env python3

import sys, os, os.path
import pwd
import glob
import re
import io 
import tempfile
import json
import yaml
import ROOT
import subprocess
import argparse
import logging
import coloredlogs

from datetime import datetime
from concurrent.futures import ThreadPoolExecutor, as_completed

logger = logging.getLogger('USTCCMS-DataDownload')
coloredlogs.install(level='DEBUG', logger=logger, fmt='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

ROOT.gROOT.SetBatch(True)



def RunFinalize(outdir, timeout, root_files):

    FileForResubmission = os.path.join(outdir, 'Zombie_files.txt')
    # Open the file in write mode ('w') to clear its contents if it exists
    f1 = open(FileForResubmission, 'w')
    for file_path in root_files:
        Zombie=False
        Missing=False
        aProb=False
        try:
            root_file = ROOT.TFile(file_path)
            if root_file.IsZombie():
                logger.warning(f"Error: File {file_path} is a Zombie.")
                Zombie =True
            #else:
            #    print(f"File {file_path} is OK.")
            root_file.Close()
        except OSError as e:
            #logger.error(f"Error: Failed to open file {file_path}. {str(e)}")
            aProb=True

        if not os.path.exists(file_path):
            #logger.error(f"Error: File {file_path} does not exist.")
            Missing=True
        
        if Missing or Zombie or aProb:
            f1.write(file_path)
            f1.write("\n")
            continue
    f1.close()
    
    # Check if the file is empty
    if os.stat(FileForResubmission).st_size == 0:
        logger.info( 'All jobs finish susccefully! ')
    else:
        logger.info( f"Found failed jobs, resubmit with: python3 download_samples.py --das {FileForResubmission} --resubmit --time {timeout} --overwrite --condor -o {outdir} ")
    return 


def decode_string(s):
    """Decode bytes array
    """
    try:
        s = s.decode('utf-8')
    except AttributeError:
        pass
    return s


def condor_submit_child_job(jobid, replicas, stoarge, timeout, outdir, overwrite=False, lazy=False):
    # Create the necessary directories
    for subdir in ["logs", "outs", "errors", "jobs"]:
        os.makedirs(os.path.join(outdir, subdir), exist_ok=True)
    
    args = f"{replicas} {stoarge} --timeout={timeout} {'--overwrite' if overwrite else ''} {'--lazy' if lazy else ''}"
    
    # Create a temporary shell script
    script_path = os.path.join(outdir, f"submission_job{jobid}.sh")
    #with tempfile.NamedTemporaryFile('w', dir=os.getcwd(), delete=False, suffix='.sh') as script_file:
    with open(script_path, "w") as script_file: 
        script_file.write(f"""#!/bin/bash

_BASE=/cvmfs/grid.cern.ch/alma9-ui-current

# Source the CMS environment setup script
source /cvmfs/cms.cern.ch/cmsset_default.sh
source $_BASE/etc/profile.d/setup-alma9-test.sh

# Verify that X509_CERT_DIR is set correctly
echo "X509_CERT_DIR is set to: $X509_CERT_DIR"
echo "X509_USER_PROXY is set to: $X509_USER_PROXY"

pushd  /ustcfs3/cms/ustc-support/rucio/
python3 process_replica.py {args}
""")
    
    # Make the script executable
    #script_path = script_file.name
    os.chmod(script_path, 0o755)
    
    condor_job_description = f"""universe = vanilla
executable = {script_path} 

log = {outdir}/logs/condor-$(Cluster)_{jobid}.log
output = {outdir}/outs/condor-$(Cluster)_{jobid}.out
error = {outdir}/errors/condor-$(Cluster)_{jobid}.err

# Transfer proxy certificate to the worker node
transfer_input_files = {X509_USER_PROXY}
should_transfer_files = YES
when_to_transfer_output = ON_EXIT
accounting_group = short

# Environment variables for the job
environment = "X509_USER_PROXY={X509_USER_PROXY}"

# Create directories if they don't exist
#on_exit_hold = (ExitBySignal == True) || (ExitCode != 0)
#on_exit_remove = (ExitBySignal == False) && (ExitCode == 0)

queue
"""
    # Define your condor job file and the environment setup commands
    condor_job = os.path.join(outdir, "jobs", f"condor_job{jobid}.submit")
    with open(condor_job, "w") as submit_file:
        submit_file.write(condor_job_description)

    # Submit the job using condor_submit
    proc = subprocess.Popen(["condor_submit", condor_job], stdout=subprocess.PIPE,stderr=subprocess.PIPE)#, shell=True)
    out, err = proc.communicate()
    if proc.returncode != 0:
        logger.error(f"Failed to submit job {jobid} with command: {script_path}")
        logger.error(f"Error output: {err.decode().strip()}")
    else:
        logger.info(f"Successfully submitted job {jobid} with command: {script_path}")
    return 


def get_rse_replica(input_string):
    lines = input_string.strip().split('\n')
    # Extract column headers
    headers = [header.strip() for header in lines[1].split('|') if header.strip()]
    print( headers)

    # Initialize dictionary to store data
    data = {header: [] for header in headers}

    # Iterate over lines starting from index 2 to skip the header lines
    for line in lines[3:]:
        # Extract values using strip() and split() methods
        values = [value.strip() for value in line.split('|') if value.strip()]
        # Populate the dictionary
        for header, value in zip(headers, values):
            data[header].append(value)

    print(data['RSE: REPLICA'])
    return data['RSE: REPLICA'] 


def download_replicas(data, stoarge, lazy=False):
    davs_file_paths = [path for path in data if 'davs://' in path]
    if lazy:
        run_over = [davs_file_paths[0]]
    else:
        run_over = davs_file_paths

    def process_replicas(replica):
        rf = 'davs:' + replica.split('davs:')[1]
        subdir = rf.split('store')[1].split('/')[1:-1]
        directory_path = os.path.join(storage, *subdir) + '/.'

        if not os.path.exists(directory_path):
            os.makedirs(directory_path)

        cmd = [f"{gfal_copy}", rf, directory_path]
        print("Processing...  {0}".format(" ".join(cmd)))

        if not os.path.exists(os.path.join(directory_path, rf.split('/')[-1])):
            process = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            if process.returncode == 0:
                return True
            else:
                print("Failed to run {0}".format(" ".join(cmd)))
                print("Will try another replica!")
                return False
        else:
            print('The file exists and overwrite is not set!')
            return True
    
    max_workers = 10  # Adjust based on your system's capability
    with ThreadPoolExecutor(max_workers=max_workers) as executor:
        future_to_replica = {executor.submit(process_replicas, replica): replica for replica in run_over}

        for future in as_completed(future_to_replica):
            replica = future_to_replica[future]
            try:
                result = future.result()
                if result:
                    break
            except Exception as exc:
                print(f'{replica} generated an exception: {exc}')
    return


def list_file_replicas(input_string):
    lines   = input_string.strip().split('\n')
    headers = [header.strip() for header in lines[2].split('|') if header.strip()]
    data    = {header: [] for header in headers}
    dict_   = {}

    for  line in lines[4:-3]:
        values = [value.strip() for value in line.split('|') if value.strip()]
        for header, value in zip(headers, values):
            data[header].append(value)
        
    for i, smp in enumerate(data['SCOPE:NAME']):
            cmd  = [f"{rucio}", "list-file-replicas", f"{smp}"]
            print( f"Processing...  {i+1} {' '.join(cmd)}")
                
            # Run the command and capture the output
            process = subprocess.run(cmd, stdout=subprocess.PIPE)
            # Decode the output bytes to string and join the lines
            output_string = "\n".join(process.stdout.decode().splitlines())
                
            replicas_per_smp = get_rse_replica(output_string)
            dict_ [f"{smp}"]=  replicas_per_smp
            print ( 'done.')
            
    for line in lines[-3:]:
        if "GB" in line:
            conv = 1024 # 1 TB = 1024 GB
        elif "MB" in line:
            conv = pow(1024,2)  # 1 TB = 1024×1024 = 1,048,576 MB
        if re.search("Total size :", line):
            size_per_smp = float(line.split()[-2])
            Totalsize = size_per_smp/conv
    return Totalsize, dict_


def run_subprocess_call(cmd, working_dir='.'):
    try:
        logger.info("Processing...  {0}".format(" ".join(cmd)))
        subprocess.check_call(cmd, cwd=working_dir)
    except subprocess.CalledProcessError as e:
        logger.error(f"Failed to run {' '.join(cmd)}: {str(e)}")
        #raise  # Re-raise the exception to propagate it further if needed
    return 

def list_files(datasets, base):
    if not os.path.exists(os.path.join(base,'request')):
        os.makedirs(os.path.join(base,'request'))
    
    files = []
    for smp in datasets:
        org_stdout = sys.stdout
        
        dataset = smp
        if isinstance(smp, tuple): dataset = smp[0]

        smpNm = "_".join(dataset.split('/')[1:-1])
        fout = f"{base}/request/{smpNm}.txt"
        f = open(fout, "w+")
        files += [fout]
        sys.stdout = f
        cmd = [f"{rucio}", "list-files", f"cms:{dataset}"]
        print (f"#{dataset}")
        f.flush()  # Flush to ensure it's physically written before subprocess output
        subprocess.call(cmd, stdout=f)
    
        sys.stdout = org_stdout
        f.close()
     
    return files


def read_datasets(file_path):
    datasets = []
    with open(file_path, 'r') as inFile:
        for line in inFile:
            stripped_line = line.strip()
            if stripped_line.startswith('#'): 
                continue
            if stripped_line:
                datasets.append(stripped_line)
    return datasets



if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-o', '--outdir', required=False, action='store', default=f"cmstransfer_{datetime.now().strftime('%Y%m%d_%H%M%S')}", help='jobs file to be stored in')
    parser.add_argument("-t","--timeout", default = "1800", help = "Sets the send/recieve timeout for the gfal command")
    parser.add_argument('--das', required=True, action='store', help=' a Text file of datasets path')
    parser.add_argument('--stoarge', required=False, action='store', default='/ustcfs3/cms/store', help=' datasets to be stored in')
    parser.add_argument('--lazy', action='store_true', default=False, help='Multiple replicas exist for single dataset, if this flagged true, will try only one replica')
    parser.add_argument('--condor', action='store_true', default=False, help='Submit to HTCondor: 1 job per dataset')
    parser.add_argument('--overwrite', action='store_true', default=False, help='If  the root file exist will pass')
    parser.add_argument('--requestsmp', action='store_true', default=False, help='Will copy DAS to the /stoarge, otherwise will only prepare the repliacs requests in /ustcfs3/cms/request')
    parser.add_argument('--resubmit', action='store_true', default=False, help='Try to copy once more, advised to set overwrite=True, and keep lazy=False')
    parser.add_argument('--finalize', action='store_true', default=False, help='check for failed jobs')

    options = parser.parse_args()

    if options.finalize:
        options.requestsmp = False
        options.resubmit = False
        options.condor = False
    else:
        if options.requestsmp:
            print("Processing requested samples ...")
            options.finalize =False
            options.resubmit =False 
        if options.resubmit:
            print("Resubmitting jobs...")
            options.overwrite = True
            options.lazy = False

    base  = '/ustcfs3/cms'
    rucio = 'rucio' 
    gfal_copy='gfal-copy'
    #rucio ='/cvmfs/cms.cern.ch/rucio/x86_64/rhel7/py3/current/bin/rucio'
    #gfal_copy='/cvmfs/grid.cern.ch/alma9-ui-current/usr/bin/gfal-copy'
    
    try:
        RUCIO_ACCOUNT = os.getenv('USER') or pwd.getpwuid(os.getuid()).pw_name
        print("RUCIO_ACCOUNT is set to:", RUCIO_ACCOUNT)
    except Exception as e:
        logger.warning(f"Failed to retrieve RUCIO_ACCOUNT: {e}")
    
    X509_USER_PROXY=os.getenv('X509_USER_PROXY') 
    print("X509_USER_PROXY is set to:", X509_USER_PROXY)
    
    datasets = read_datasets(options.das)
    #print("Processing datasets::", datasets)
    files = []
    jobid = 0
    
    if options.resubmit:
        for dataset in datasets:
            dataset = dataset.replace("/ustcfs3/cms", "").replace('//', '/')
            rf =  list(filter(bool, dataset.split('/'))) 

            json_request = os.path.join("/ustcfs3/cms/", 'request', f'{rf[3]}_{rf[2]}-{rf[5]}.json')
            print ("Getting request from:", json_request)
            
            with open(json_request, "r") as json_file:
                replicas_per_smp = json.load(json_file)
             
            replica_per_rf = replicas_per_smp['cms:'+dataset]
            
            if  options.condor:
                condor_submit_child_job(jobid, 
                                        replica_per_rf, 
                                        stoarge   = options.stoarge, 
                                        timeout   = options.timeout, 
                                        outdir    = options.outdir, 
                                        overwrite = options.overwrite, 
                                        lazy      = options.lazy)
                jobid +=1
            else:
                cmd = ["python3", "process_replica.py", f"{replica_per_rf} {options.stoarge} {options.timeout} {'--overwrite' if options.overwrite else ''} {'--lazy' if options.lazy else ''}"]
                run_subprocess_call(cmd)
        print (f" ==> Full list of samples are queued for resubmission: {options.das}" ) 
    
    else:
        files = list_files(datasets, base=base)

        results =[] 
        Totalsize = 0
        for file_path in files:
            
            if os.path.getsize(file_path) == 0:
                continue
            if file_path.startswith('#'):
                continue

            input_string = ""
            with open(file_path, "r") as file:
                input_string = file.read()
        
            json_request = file_path.replace('.txt', '.json')
            print ("Getting request from:", json_request)
            if not os.path.exists(json_request):
                size_per_smp, replicas_per_smp= list_file_replicas(input_string)
                Totalsize += size_per_smp
                print (f'{file_path} size {size_per_smp} TB')
                print ('------------------'*5) 
                with open(json_request, "w+") as json_file:
                    json.dump(replicas_per_smp, json_file, indent=4) 
            else:
                with open(json_request, "r") as json_file:
                    replicas_per_smp = json.load(json_file)
            
            if options.finalize:
                results += [ das.replace('cms:','/ustcfs3/cms/') for das in replicas_per_smp.keys() ]

            elif options.requestsmp:
                for root_file, replica_per_rf in replicas_per_smp.items():
                    if  options.condor:
                        condor_submit_child_job(jobid, 
                                                replica_per_rf, 
                                                stoarge   = options.stoarge, 
                                                timeout   = options.timeout, 
                                                outdir    = options.outdir, 
                                                overwrite = options.overwrite, 
                                                lazy      = options.lazy)
                        jobid +=1
                    else:
                        cmd = ["python3", "process_replica.py", f"{replica_per_rf} {options.stoarge} {options.timeout} {'--overwrite' if options.overwrite else ''} {'--lazy' if options.lazy else ''}"]
                        run_subprocess_call(cmd)
        print (f" ==> Full list of samples size: {Totalsize} TB" ) 
        
        if options.finalize:
            RunFinalize(options.outdir, options.timeout, results)




