#!/usr/bin/env python3

import os, sys
import argparse
import subprocess


def process_replica(replica, stoarge, gfal_copy, timeout, overwrite=False, lazy=False):
    rf = 'davs:' + replica.split('davs:')[1].replace(',', '')
    subdir = rf.split('store')[1].split('/')[1:-1]
    directory_path = os.path.join(stoarge, *subdir) + '/.'

    if not os.path.exists(directory_path):
        os.makedirs(directory_path)
    
    cmd = [gfal_copy, f"-t={timeout}", rf, directory_path]
    if overwrite:
        cmd = [gfal_copy, "-f", f"-t={timeout}", rf, directory_path]

    if not os.path.exists(os.path.join(directory_path, rf.split('/')[-1])) or overwrite:
        print("=" * 8)
        print("Processing...  {0}".format(" ".join(cmd)))
        if lazy:
            # Run the command in the background and try only one replica
            process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        else:
            process = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = process.stdout, process.stderr
            if process.returncode == 0:
                print(f"Transfer completed successfully!")
                return True
            else:
                print("Failed to run {0}".format(" ".join(cmd)))
                print(f"Standard Output:\n{stdout.decode('utf-8')}")
                print(f"Standard Error:\n{stderr.decode('utf-8')}")
                print("Will try another replica!")
                return False
    else:
        print('The file exists and overwrite is not set!')
        return True

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process replica files.')
    parser.add_argument('replicas_list', nargs='+', help='List of replicas to process')
    parser.add_argument('storage_path', help='Path to the storage')
    parser.add_argument('--overwrite', action='store_true', help='Overwrite existing files')
    parser.add_argument('--lazy', action='store_true', help='Lazy mode')
    parser.add_argument("-t","--timeout", default = "1800", help = "Sets the send/recieve timeout for the gfal command")
    
    args = parser.parse_args()

    gfal_copy='/cvmfs/grid.cern.ch/alma9-ui-current/usr/bin/gfal-copy'

    davs_file_paths = [path for path in args.replicas_list if 'davs://' in path]
    if args.lazy: run_over= [davs_file_paths[0]]
    else: run_over= davs_file_paths

    successful = False
    for replica in run_over:
        successful = process_replica(replica, args.storage_path, gfal_copy, timeout=args.timeout, overwrite=args.overwrite, lazy=args.lazy)
        if successful:
            break

    if successful:
        sys.exit(0)
    else:
        sys.exit(1)

