#/NMSSM_XtoYHto4B_MX-3000_MY-100_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer23NanoAODv12-130X_mcRun3_2023_realistic_v15-v2/NANOAODSIM
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
| SCOPE:NAME                                                                                                                                                                                          | GUID   | ADLER32     | FILESIZE   | EVENTS   |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------|
| cms:/store/mc/Run3Summer23NanoAODv12/NMSSM_XtoYHto4B_MX-3000_MY-100_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_v15-v2/2560000/5ec7eada-0e25-4e19-a3d7-ac7d9a8350ae.root | (None) | ad:bbbb2fcf | 38.206 MB  |          |
| cms:/store/mc/Run3Summer23NanoAODv12/NMSSM_XtoYHto4B_MX-3000_MY-100_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_v15-v2/2560000/64c491a2-c757-48f6-9200-2e2c72e1ff64.root | (None) | ad:d50cd2d4 | 160.795 MB |          |
| cms:/store/mc/Run3Summer23NanoAODv12/NMSSM_XtoYHto4B_MX-3000_MY-100_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_v15-v2/2560000/145fd14e-a6ee-44cd-8d5a-f6b0165ac863.root | (None) | ad:9affef23 | 5.808 MB   |          |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
Total files : 3
Total size : 204.809 MB
