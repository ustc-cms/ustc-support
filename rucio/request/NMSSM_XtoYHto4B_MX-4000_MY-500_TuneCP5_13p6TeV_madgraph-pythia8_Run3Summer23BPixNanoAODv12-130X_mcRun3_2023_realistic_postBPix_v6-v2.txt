#/NMSSM_XtoYHto4B_MX-4000_MY-500_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer23BPixNanoAODv12-130X_mcRun3_2023_realistic_postBPix_v6-v2/NANOAODSIM
+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
| SCOPE:NAME                                                                                                                                                                                                     | GUID   | ADLER32     | FILESIZE   | EVENTS   |
|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------|
| cms:/store/mc/Run3Summer23BPixNanoAODv12/NMSSM_XtoYHto4B_MX-4000_MY-500_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_postBPix_v6-v2/140000/51901ff6-9493-47a2-82e1-714068a8bdb2.root | (None) | ad:1c10224e | 9.684 MB   |          |
| cms:/store/mc/Run3Summer23BPixNanoAODv12/NMSSM_XtoYHto4B_MX-4000_MY-500_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_postBPix_v6-v2/140000/33797768-f89e-4dcb-9092-12347f91bb96.root | (None) | ad:3c54a0c3 | 100.390 MB |          |
| cms:/store/mc/Run3Summer23BPixNanoAODv12/NMSSM_XtoYHto4B_MX-4000_MY-500_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_postBPix_v6-v2/140000/4dda060d-1b14-4fcd-a6c3-1b165be720e7.root | (None) | ad:0227d3b7 | 6.145 MB   |          |
+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
Total files : 3
Total size : 116.218 MB
