#/NMSSM_XtoYHto4B_MX-800_MY-200_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer23NanoAODv12-130X_mcRun3_2023_realistic_v15-v2/NANOAODSIM
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
| SCOPE:NAME                                                                                                                                                                                        | GUID   | ADLER32     | FILESIZE   | EVENTS   |
|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------|
| cms:/store/mc/Run3Summer23NanoAODv12/NMSSM_XtoYHto4B_MX-800_MY-200_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_v15-v2/130000/649e92af-88b6-47f2-b268-2df1532ba542.root | (None) | ad:4346cdec | 10.987 MB  |          |
| cms:/store/mc/Run3Summer23NanoAODv12/NMSSM_XtoYHto4B_MX-800_MY-200_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_v15-v2/130000/e7469194-efb4-4f0b-80b4-8097238e101e.root | (None) | ad:4dc3ee72 | 168.872 MB |          |
| cms:/store/mc/Run3Summer23NanoAODv12/NMSSM_XtoYHto4B_MX-800_MY-200_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_v15-v2/130000/ed6c3034-bb50-4fff-8c20-ffd73732d1a0.root | (None) | ad:6f8b85e9 | 5.525 MB   |          |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
Total files : 3
Total size : 185.384 MB
