# USTC-Support

This Gitlab project is currently a work in progress. Its purpose is to offer assistance to users affiliated with USTC who are involved in the CMS experiment. If you encounter any difficulties using any cluster (ihep, lxplus, ustc) or dealing with packages related to your work, we strongly recommend opening an issue here: https://gitlab.cern.ch/ustc-cms/ustc-support/-/issues

Our aim is that the issues you raise may benefit others in the future, and in turn, we can collectively provide support to each other.

Additionally, you have the option to add or mirror any of your publicly available repositories under the USTC-CMS project at https://gitlab.cern.ch/ustc-cms. 

You can also share any valuable scripts on the USTC-CMS/USTC-Support Gitlab at https://gitlab.cern.ch/ustc-cms/ustc-support.
If you want to contribute, please feel free to request access from the repositry owners. You will get an invitation once we have granted you permission and then be able to create and edit pages here.