#/NMSSM_XtoYHto4B_MX-300_MY-60_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22NanoAODv12-130X_mcRun3_2022_realistic_v5-v2/NANOAODSIM
+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
| SCOPE:NAME                                                                                                                                                                                     | GUID   | ADLER32     | FILESIZE   | EVENTS   |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------|
| cms:/store/mc/Run3Summer22NanoAODv12/NMSSM_XtoYHto4B_MX-300_MY-60_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/60000/013fab63-6a1e-4c4e-817f-ba9a8efe7c7c.root | (None) | ad:96d5bf31 | 4.233 MB   |          |
| cms:/store/mc/Run3Summer22NanoAODv12/NMSSM_XtoYHto4B_MX-300_MY-60_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/60000/6caba5c7-6eff-4de9-bcd6-47a508eb6a0c.root | (None) | ad:68392e10 | 3.725 MB   |          |
| cms:/store/mc/Run3Summer22NanoAODv12/NMSSM_XtoYHto4B_MX-300_MY-60_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/60000/0610ce32-95da-4f39-ab43-45d48cac3521.root | (None) | ad:188580c9 | 39.654 MB  |          |
| cms:/store/mc/Run3Summer22NanoAODv12/NMSSM_XtoYHto4B_MX-300_MY-60_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/60000/59f6fec6-f8e4-4f73-a45d-c82c3ed371eb.root | (None) | ad:001d56d8 | 4.242 MB   |          |
+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
Total files : 4
Total size : 51.854 MB
