#/NMSSM_XtoYHto4B_MX-1000_MY-125_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22NanoAODv12-130X_mcRun3_2022_realistic_v5-v2/NANOAODSIM
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
| SCOPE:NAME                                                                                                                                                                                       | GUID   | ADLER32     | FILESIZE   | EVENTS   |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------|
| cms:/store/mc/Run3Summer22NanoAODv12/NMSSM_XtoYHto4B_MX-1000_MY-125_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/60000/d71c55fc-efcb-43df-a447-1811a4210e5a.root | (None) | ad:fddb105e | 3.831 MB   |          |
| cms:/store/mc/Run3Summer22NanoAODv12/NMSSM_XtoYHto4B_MX-1000_MY-125_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/60000/13178cea-f591-423a-a7fe-bdc5e985058c.root | (None) | ad:f3e3b842 | 60.984 MB  |          |
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
Total files : 2
Total size : 64.814 MB
