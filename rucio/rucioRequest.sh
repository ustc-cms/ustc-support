#!/bin/bash
# original author: Khawla Jaffel
source /cvmfs/cms.cern.ch/cmsset_default.sh 
# Ensure the voms-proxy-init command is available
VOMS_PROXY_INIT_CMD=$(which voms-proxy-init)
if [ -x "$VOMS_PROXY_INIT_CMD" ]; then
    # Use the existing proxy certificate
    export X509_USER_PROXY=/tmp/x509up_u$(id -u) #does not work on htcondor
else
    echo "voms-proxy-init command not found."
    echo "let's setup your VOMS-PROXY !"
    voms-proxy-init -voms cms -rfc -valid 192:00
fi
source /cvmfs/cms.cern.ch/rucio/setup-py3.sh
export RUCIO_ACCOUNT=`whoami`
echo "Hello $RUCIO_ACCOUNT !"

echo """
    =====================================================================================================    
    - You can issue the command  
        $ rucio --help  # to understand all your options. 
    - You can list your quota at all sites via the command 
        $ rucio list-account-limits $ RUCIO_ACCOUNT.
    - If you do not have quota in the right place (or enough), you should ask for a quota increase at 
    the RSE(Rucio Storage Elements) you would like to use. 
    To find out who to ask, you can query the RSE attributes to identify the accounts responsible for managing quota.
        $ rucio list-rse-attributes T2_CN_Beijing
    - know more about Rucio here: https://twiki.cern.ch/twiki/bin/view/CMSPublic/Rucio
                                : https://twiki.cern.ch/twiki/bin/view/CMSPublic/RucioUserDocsRules
				: https://twiki.cern.ch/twiki/bin/view/CMSPublic/RucioUserDocsContainers
    =====================================================================================================    
"""

useContainer=true
site=T2_CN_Beijing

#=======================================================================================
#  Create a User Container 
#=======================================================================================
container='hh4b'

#declare -a requested_samples=($(cat requests/rucio_hh4b.txt | tr '\n' ' '))

requested_samples=(
/JetHT/Run2022A-ReRecoNanoAODv11-v1/NANOAOD
/JetHT/Run2022B-ReRecoNanoAODv11-v1/NANOAOD
/JetHT/Run2022C-PromptNanoAODv10-v1/NANOAOD
/JetHT/Run2022C-PromptNanoAODv10_v1-v1/NANOAOD
/JetHT/Run2022C-ReRecoNanoAODv11-v1/NANOAOD
/ZZ_TuneCP5_13p6TeV_pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM
/WZ_TuneCP5_13p6TeV_pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM
/WW_TuneCP5_13p6TeV_pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM
/GluGluHto2B_PT-200_M-125_TuneCP5_13p6TeV_powheg-minlo-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM
/ZH_Hto2B_Zto2Q_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM
/ggZH_Hto2B_Zto2Q_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM
/WminusH_Hto2B_Wto2Q_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM
)

if $useContainer; then
    #=======================================================================================
    # ! Create a new container ( 1st time only )
    #=======================================================================================
    rucio add-container user.$RUCIO_ACCOUNT:/Analyses/$container/USER
fi

for smp in ${requested_samples[*]}; do 
    if $useContainer; then
        #=======================================================================================
        # !  Add some initial datasets to the Container 
        #=======================================================================================
        rucio attach user.$RUCIO_ACCOUNT:/Analyses/$container/USER cms:$smp
    else
        #=======================================================================================
        # ! You have enough quota:: just go ahead with the command below
        rucio add-rule cms:$smp 1 $site
        #=======================================================================================
        # ! To be used when you don't have enouh quota to make the request 
        #rucio add-rule --ask-approval cms:$smp 1 $site
        #=======================================================================================
    fi
done

if $useContainer; then
    #=======================================================================================
    # ! Subscribe/Transfer the container to a site
    #=======================================================================================
    rucio add-rule --account=$RUCIO_ACCOUNT user.$RUCIO_ACCOUNT:/Analyses/$container/USER 1 $site
    #=======================================================================================
    # ! Check the current contents of the container 
    #=======================================================================================
    rucio list-content user.$RUCIO_ACCOUNT:/Analyses/$container/USER 
fi

#=======================================================================================
# ! Check status of transfered datasets
#=======================================================================================
rucio list-rules --account=$RUCIO_ACCOUNT | grep "REPLICATING"
