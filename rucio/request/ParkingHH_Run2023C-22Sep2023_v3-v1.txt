+--------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
| SCOPE:NAME                                                                                                   | GUID   | ADLER32     | FILESIZE   | EVENTS   |
|--------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------|
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/6494d8e7-4a1b-4102-a2f9-14f324b395ad.root | (None) | ad:5777052f | 1.129 GB   |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/963af7a8-baae-415d-95e7-cc69d6dd49b3.root | (None) | ad:9de768ee | 1.160 GB   |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/f7dad222-0125-4974-b9d1-240c81cccb6c.root | (None) | ad:7a1e0724 | 351.098 MB |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/05dbdfad-e54b-4f55-b718-335522488a92.root | (None) | ad:39b66b41 | 301.495 MB |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/9501adda-4973-48c1-ae66-c27c6ca3d8c7.root | (None) | ad:9cd12ef7 | 803.082 MB |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/fa272f03-4b94-4e24-ae8d-1b9ae1388830.root | (None) | ad:97dafea0 | 1.089 GB   |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2550000/6f36c3d0-5ff0-4207-86ba-10afa7a2454b.root | (None) | ad:77988cbe | 355.220 MB |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/60000/ba55316d-69d0-4fb4-bf5c-403d50ba32ba.root   | (None) | ad:a70ca475 | 870.931 MB |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/13eee9d0-b70a-46fb-b8a9-38bdcd9e1c42.root | (None) | ad:7a16bb34 | 236.317 MB |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/284d94cd-da30-408d-ab97-3b867ec18d87.root | (None) | ad:34cebc9a | 1.093 GB   |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/4ff9623f-99f0-4089-b516-f89ca11c9a69.root | (None) | ad:ced39952 | 986.887 MB |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/883cb265-e067-455e-92d2-3822dbee6a67.root | (None) | ad:70e333bb | 1.061 GB   |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/be9d5cc0-3a3d-456b-a935-baa7cc6ba966.root | (None) | ad:19cd4eea | 1.060 GB   |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/c6fe44cf-8b9d-4730-9a39-4edc2863c069.root | (None) | ad:985643e8 | 1.073 GB   |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/f357b142-08cc-47d4-b76f-ccda78f9d725.root | (None) | ad:2e8a2ab1 | 1.230 GB   |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/f846f01a-793b-471c-939b-d865505fc4c0.root | (None) | ad:6621ac5b | 1.125 GB   |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2550000/892b058e-a74e-446c-9038-a6953db13a7b.root | (None) | ad:5ce9efbe | 598.354 MB |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/87b9fde0-9a43-436b-888e-9ad24d9caf9b.root | (None) | ad:015634ff | 29.888 MB  |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2550000/d94bf63b-6150-443c-95cd-d7d23d8dd844.root | (None) | ad:872b8d61 | 34.934 MB  |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/60000/260a15ae-728f-4702-a504-d3e9dfbc7b34.root   | (None) | ad:fbe97753 | 58.819 MB  |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/610000/0b36184d-cd6d-4618-abfd-175b45ba47eb.root  | (None) | ad:282075cf | 1.153 GB   |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/98c3bedc-49b5-4ac4-bb9c-1955c7b67c42.root | (None) | ad:1cf77b37 | 468.947 MB |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/a5ce1bde-7563-4eee-a6e1-9f47a28f4b76.root | (None) | ad:d5f367ec | 1.053 GB   |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/c7538417-f9c6-4a3d-bde7-aba16b076e5b.root | (None) | ad:22ceb490 | 1.062 GB   |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/ece9167d-fd7f-4a30-aba0-635be8df9625.root | (None) | ad:f55f8a1a | 1.063 GB   |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/d6ad84bb-6e74-4bdd-beed-25f493d92c77.root | (None) | ad:2770cf00 | 1.128 GB   |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2550000/1bbd7651-09cc-4ea3-a693-d155cd9f444a.root | (None) | ad:21c76239 | 42.418 MB  |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2550000/5a794424-0bf5-44e4-9a63-f8d5a9ab1cf0.root | (None) | ad:b1eef141 | 43.607 MB  |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2550000/165f04d0-cccb-47dc-92bd-88c43f4c0087.root | (None) | ad:ff85ed98 | 579.043 MB |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/dbf10fbc-ac78-42ab-8e2c-fbf16dabc5cd.root | (None) | ad:4aaaf82a | 326.043 MB |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/70499de4-5850-44b5-aa8c-02655ec05b23.root | (None) | ad:b2d98380 | 319.946 MB |          |
| cms:/store/data/Run2023C/ParkingHH/NANOAOD/22Sep2023_v3-v1/2560000/972bf226-7ff3-4a83-81ae-7dbc7a4b5c60.root | (None) | ad:9c735417 | 1.058 GB   |          |
+--------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
Total files : 32
Total size : 22.944 GB
