+-------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
| SCOPE:NAME                                                                                                  | GUID   | ADLER32     | FILESIZE   | EVENTS   |
|-------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------|
| cms:/store/data/Run2022B/MuonEG/NANOAOD/ReRecoNanoAODv11-v1/50000/1b665cfc-5d89-4067-a381-9b4bc35a7ff9.root | (None) | ad:66277ff8 | 3.424 MB   |          |
| cms:/store/data/Run2022B/MuonEG/NANOAOD/ReRecoNanoAODv11-v1/50000/5f14101a-4c99-4f89-974b-6c1cd6ebc65d.root | (None) | ad:0ce57ac9 | 156.115 MB |          |
| cms:/store/data/Run2022B/MuonEG/NANOAOD/ReRecoNanoAODv11-v1/50000/6e3447a1-83b9-4e10-83a4-25781d92a9b5.root | (None) | ad:c235d2eb | 23.778 MB  |          |
| cms:/store/data/Run2022B/MuonEG/NANOAOD/ReRecoNanoAODv11-v1/50000/8a09945a-27de-47d8-a552-af74712bd416.root | (None) | ad:ec864617 | 3.460 MB   |          |
| cms:/store/data/Run2022B/MuonEG/NANOAOD/ReRecoNanoAODv11-v1/50000/c5e21cc5-db87-4d21-9ffd-b6f3df2d030f.root | (None) | ad:d159abdb | 63.812 MB  |          |
| cms:/store/data/Run2022B/MuonEG/NANOAOD/ReRecoNanoAODv11-v1/50000/f08ed1de-f148-494a-a773-67fe4a205622.root | (None) | ad:487fb42b | 57.114 MB  |          |
+-------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
Total files : 6
Total size : 307.702 MB
