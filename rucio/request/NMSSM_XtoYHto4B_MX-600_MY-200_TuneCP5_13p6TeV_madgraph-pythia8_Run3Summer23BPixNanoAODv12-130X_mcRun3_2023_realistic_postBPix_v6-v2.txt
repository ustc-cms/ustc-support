#/NMSSM_XtoYHto4B_MX-600_MY-200_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer23BPixNanoAODv12-130X_mcRun3_2023_realistic_postBPix_v6-v2/NANOAODSIM
+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
| SCOPE:NAME                                                                                                                                                                                                     | GUID   | ADLER32     | FILESIZE   | EVENTS   |
|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------|
| cms:/store/mc/Run3Summer23BPixNanoAODv12/NMSSM_XtoYHto4B_MX-600_MY-200_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_postBPix_v6-v2/2810000/4bd30b71-22bf-46ce-a5fd-f9fb63c822b5.root | (None) | ad:b56ccbc9 | 68.940 MB  |          |
| cms:/store/mc/Run3Summer23BPixNanoAODv12/NMSSM_XtoYHto4B_MX-600_MY-200_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_postBPix_v6-v2/2810000/b2189eb7-45e9-42ff-848b-543760592071.root | (None) | ad:4acbe1cb | 10.436 MB  |          |
| cms:/store/mc/Run3Summer23BPixNanoAODv12/NMSSM_XtoYHto4B_MX-600_MY-200_TuneCP5_13p6TeV_madgraph-pythia8/NANOAODSIM/130X_mcRun3_2023_realistic_postBPix_v6-v2/2810000/c8606525-521e-4a09-b85b-70131b8242ba.root | (None) | ad:33ea46e6 | 10.395 MB  |          |
+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
Total files : 3
Total size : 89.772 MB
