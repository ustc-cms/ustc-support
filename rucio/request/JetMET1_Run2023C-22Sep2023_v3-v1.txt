+------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
| SCOPE:NAME                                                                                                 | GUID   | ADLER32     | FILESIZE   | EVENTS   |
|------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------|
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/17feb204-0505-49cf-9813-002c0b62e19b.root   | (None) | ad:5faa6356 | 68.819 MB  |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/c20db6ef-8928-43c8-bb45-13373e0e384d.root   | (None) | ad:6e89aa80 | 38.287 MB  |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/af851760-b3ac-45e3-9e3e-a455301cba30.root   | (None) | ad:326ef58f | 942.740 MB |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/cf13f6a9-fa02-4bb7-a180-3c1ab9484909.root   | (None) | ad:cf4a9d3d | 136.385 MB |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/40000/23736ed0-8025-41cd-a585-e2cf4e201c8c.root   | (None) | ad:35ca1676 | 1.078 GB   |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/40000/e0c54b3a-30b1-48d7-ab12-56e5d57b5ff3.root   | (None) | ad:f8957505 | 309.575 MB |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/58c05c81-4182-45dd-b319-e70724cba67e.root   | (None) | ad:82fcabee | 573.987 MB |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/40000/3e72db12-9c4a-4a35-a737-0b07d29b2220.root   | (None) | ad:47e27082 | 111.437 MB |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/2540000/e563f0d9-53ad-4023-94c2-b7c8dbc50c00.root | (None) | ad:29a26c42 | 68.689 MB  |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/40000/31f7e69b-0f21-4b00-986d-aedd9189162a.root   | (None) | ad:feda0051 | 494.584 MB |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/40000/ab58fc34-6194-484a-b2cb-3caaac448427.root   | (None) | ad:e31ac341 | 236.743 MB |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/40000/6c11b77f-8db2-4f38-b5ef-b74969652abf.root   | (None) | ad:2013bcce | 380.968 MB |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/40000/fc88ead0-ae2f-4fdb-b396-718a348ade6d.root   | (None) | ad:a3d479e4 | 1.010 GB   |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/0f41bfc8-00f2-4a07-ad02-d6a27955041a.root   | (None) | ad:ed7b6a46 | 254.920 MB |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/ab696ccf-36f8-4dc9-977c-ca2e042e3a5b.root   | (None) | ad:3130ae36 | 1.372 GB   |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/ad28243e-324e-4ec5-a397-be28ad005f88.root   | (None) | ad:73343efd | 949.912 MB |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/185b7f84-0e77-4871-b902-c067f7404edf.root   | (None) | ad:eae8e760 | 269.204 MB |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/40000/6c9417fe-1cdc-4be7-a818-69927a49993b.root   | (None) | ad:ebad8ed3 | 145.812 MB |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/243554ba-de8c-4198-ac4d-f09be977ba7d.root   | (None) | ad:39e3cac0 | 998.447 MB |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/4799fa9c-5bbb-42c1-964d-4fa7565f8f37.root   | (None) | ad:92a601cb | 1.443 GB   |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/a88a61ee-a671-44dd-8bfc-5d26b66fe812.root   | (None) | ad:e1bdadde | 1.725 GB   |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/db0f9db8-5265-4502-a91d-82b781e704c1.root   | (None) | ad:7a7fd5b9 | 882.261 MB |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/43eecfab-be1c-40b4-9f1d-8270e101c564.root   | (None) | ad:2b9fa9d3 | 1.098 GB   |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/496786a6-ba31-4515-a38b-6af87a4cc362.root   | (None) | ad:e25a927d | 211.836 MB |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/7a98a126-d14b-4a74-a348-fcf0acb5d5ae.root   | (None) | ad:64bf439d | 1.013 GB   |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/965dade3-8c1f-453f-a088-9589f5d536a8.root   | (None) | ad:e92ad6ab | 1.002 GB   |          |
| cms:/store/data/Run2023C/JetMET1/NANOAOD/22Sep2023_v3-v1/30000/a7c06d2c-8c7e-438e-bbc0-1acfc3c1d4c7.root   | (None) | ad:87d17f46 | 1.393 GB   |          |
+------------------------------------------------------------------------------------------------------------+--------+-------------+------------+----------+
Total files : 27
Total size : 18.208 GB
