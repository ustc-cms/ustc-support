import ROOT
import os
import json
import glob
import subprocess
import logging
import coloredlogs

from datetime import datetime

logger = logging.getLogger('USTCCMS-DataDownload')
coloredlogs.install(level='DEBUG', logger=logger, fmt='%(asctime)s - %(name)s - %(levelname)s - %(message)s')



def checklocalfiles(era, list, s): 
    with open("missing_files.txt","w+") as outf:
        for dbLoc in list:
            dasQuery   = f"'file dataset={dbLoc.decode('utf-8')}'"
            localgoCmd = ["dasgoclient", "-query", dasQuery]
            try:
                print(f"Querying DAS: {' '.join(localgoCmd)}")
                ls_files = [ln.strip() for ln in subprocess.check_output(localgoCmd, stderr=subprocess.STDOUT).decode().split()]
            except subprocess.CalledProcessError:
                logger.error("Failed to run {0}".format(" ".join(localgoCmd)))
            
            files = glob.glob(os.path.join(f''+ os.path.dirname(ls_files[0]), '*.root'))
            if not files or len(files) != len(ls_files):
                outf.write(f"{dbLoc.decode('utf-8')}\n")
    outf.close()


def check_root_files(file_list):
    f1 = open('Zombie_files.txt', 'w+')
    for file_path in file_list:
        try:
            root_file = ROOT.TFile(file_path)
            if root_file.IsZombie():
                print(f"Error: File {file_path} is a Zombie.")
                f1.write(file_path)
                f1.write("\n")
            #else:
            #    print(f"File {file_path} is OK.")
            root_file.Close()
        except OSError as e:
            print(f"Error: Failed to open file {file_path}. {str(e)}")
            f1.write(file_path)
            f1.write("\n")
    f1.close()

if __name__ == "__main__":
    # List of ROOT files to check to check for zombies
    check = 'Run3Summer22*NanoAODv12/DYto2L-2Jets_MLL-50_TuneCP5_13p6TeV_amcatnloFXFX*/NANOAODSIM/*/'
    

    root_files = glob.glob( os.path.join('/ustcfs3/cms/store', 'mc', check, "*", "*.root"))
    #print ( root_files ) 
    #check_root_files(root_files)

    # full dataset to check for missing files
    fullrequst = []
    for root_file  in root_files:
        root_file = root_file.replace("/ustcfs3/cms/", "")
        rf =  root_file.split('/')
        json_request = os.path.join("/ustcfs3/cms/", 'request', f'{rf[3]}_{rf[2]}-{rf[5]}.json')
        fullrequst +=[json_request]
    
    f2 = open('missing_files.txt', 'w+') 
    for jsreq in list(set(fullrequst)): 
        with open(jsreq, "r") as json_file:
            replicas_per_smp = json.load(json_file)
        
        for inFile in replicas_per_smp.keys():
            inFile = inFile.replace('cms:','/ustcfs3/cms/')
            if not os.path.exists(inFile):
                print(f"Error: File {inFile} does not exist.")
                f2.write(inFile)
                f2.write("\n")
                continue
    f2.close()
    

